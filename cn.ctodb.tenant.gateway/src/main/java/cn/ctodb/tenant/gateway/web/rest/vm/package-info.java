/**
 * View Models used by Spring MVC REST controllers.
 */
package cn.ctodb.tenant.gateway.web.rest.vm;
