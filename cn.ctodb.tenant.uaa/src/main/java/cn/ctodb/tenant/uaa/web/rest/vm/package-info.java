/**
 * View Models used by Spring MVC REST controllers.
 */
package cn.ctodb.tenant.uaa.web.rest.vm;
